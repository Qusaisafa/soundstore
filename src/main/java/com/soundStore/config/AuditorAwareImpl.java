package com.soundStore.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import com.soundStore.auth.entity.User;
import com.soundStore.auth.service.UserService;



public class AuditorAwareImpl implements AuditorAware<User> {

	@Autowired
	private UserService userService;

	@Override
	public User getCurrentAuditor() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return userService.findUserByUserName(username);
	}
}
