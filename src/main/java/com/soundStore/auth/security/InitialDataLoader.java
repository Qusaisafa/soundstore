//package com.soundStore.auth.security;
//
//import java.util.Date;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationListener;
//import org.springframework.context.event.ContextRefreshedEvent;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Component;
//
//import com.soundStore.auth.entity.Authority;
//import com.soundStore.auth.entity.AuthorityName;
//import com.soundStore.auth.entity.User;
//import com.soundStore.auth.repository.UserRepository;
//import com.soundStore.auth.service.AuthorityService;
//import com.soundStore.model.Actor;
//import com.soundStore.model.Cart;
//import com.soundStore.model.Category;
//import com.soundStore.model.Order;
//import com.soundStore.model.Voice;
//import com.soundStore.repository.ActorRepository;
//import com.soundStore.repository.CartRepository;
//import com.soundStore.repository.OrderRepository;
//import com.soundStore.repository.VoiceRepository;
//
//@Component
//public class InitialDataLoader implements
//ApplicationListener<ContextRefreshedEvent> {
//	
//	@Autowired
//	private UserRepository userRepository;
//	
//	@Autowired
//	private CartRepository cartRepository;
//	
//	@Autowired
//	private ActorRepository actorRepositor;
//	
//	@Autowired
//	private OrderRepository orderRepositor;
//	
//	@Autowired
//	private AuthorityService authorityService;
//	
//	@Autowired
//	private VoiceRepository voiceRepository;
//	
//	@Autowired
//	private PasswordEncoder passwordEncoder;
//	
//	@Override
//	public void onApplicationEvent(ContextRefreshedEvent arg0) {
//		// TODO Auto-generated method stub
//		User user = new User();
//		user.setEmail("test@test.com");
//		user.setEnabled(true);
//		user.setName("test");
//		user.setLastPasswordResetDate(new Date());
//		user.setSurname("test");
//		user.setPassword(passwordEncoder.encode("12345"));
//		user.setUsername("test");
//
//		Cart cart = new Cart();
//		cart.setUser(user);
//		user.setCart(cart);
//		user = this.userRepository.save(user);
//		cart = this.cartRepository.save(cart);
//		
//		Authority auth = new Authority();
//		auth.setName(AuthorityName.ROLE_USER);
//		auth.getUsers().add(user);
//		user.getAuthorities().add(auth);
//		authorityService.create(auth);
//		
//		User admin = new User();
//		admin.setEmail("admin@admin.ps");
//		admin.setEnabled(true);
//		admin.setName("admin");
//		admin.setLastPasswordResetDate(new Date());
//		admin.setSurname("admin");
//		admin.setPassword(passwordEncoder.encode("12345"));
//		admin.setUsername("admin@admin.ps");
////		admin = userRepository.save(admin);
//		
//		Authority authAdmin = new Authority();
//		authAdmin.setName(AuthorityName.ROLE_ADMIN);
//		authAdmin.getUsers().add(admin);
//		admin.getAuthorities().add(authAdmin);
//		authorityService.create(authAdmin);
//		
//		
//		Actor actor = new Actor();
//		actor.setName("actor1");
//		actor.setAge("20");
//		actor.setGender("male");
//		this.actorRepositor.save(actor);
//		
//		Voice voice = new Voice();
//		voice.setActor(actor);
//		voice.setCategory(Category.NEWS);
//		voice.setUrl("/nnn");
//		voice.setDescription("lalala");
//		this.voiceRepository.save(voice);
//		
//		Order order = new Order();
//		order.setCart(cart);
//		order.setCompleted(false);
//		order.setUser(user);
//		order.setPaied(false);
//		order.setDocUrl("//");
//		order.setPrice(122);
//		this.orderRepositor.save(order);
//		
//		
//		
//		
//	}
//
//}
