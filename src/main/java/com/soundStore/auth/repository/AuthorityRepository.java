package com.soundStore.auth.repository;

import org.springframework.data.repository.CrudRepository;

import com.soundStore.auth.entity.Authority;


public interface AuthorityRepository  extends CrudRepository<Authority , Long> {
}