package com.soundStore.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soundStore.auth.entity.Authority;
import com.soundStore.auth.repository.AuthorityRepository;


@Service
public class AuthorityService {

	private AuthorityRepository authorityRepository;

	@Autowired
	public AuthorityService(AuthorityRepository authorityRepository) {
		this.authorityRepository = authorityRepository;	
	}

	@Transactional
	public void create(Authority authority) {
		this.authorityRepository.save(authority);
	}
}
