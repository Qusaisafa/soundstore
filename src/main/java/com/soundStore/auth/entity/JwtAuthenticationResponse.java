package com.soundStore.auth.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephan on 20.03.16.
 */
public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;
    
    private final List<String> role;

    public JwtAuthenticationResponse(String token,  List<String> role) {
        this.token = token;
        this.role = role;
    }

    public String getToken() {
        return this.token;
    }
    
    public List<String> getRole() {
    		return this.role;
    }
}