package com.soundStore.auth.entity;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}