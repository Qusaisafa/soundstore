package com.soundStore.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.soundStore.auth.security.JwtTokenUtil;
import com.soundStore.dto.ActorDto;
import com.soundStore.dto.OrderDto;
import com.soundStore.dto.OrderResponseDto;
import com.soundStore.model.Order;
import com.soundStore.service.OrderService;

@Controller
@RequestMapping(value="/api/order")

public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	@Value("${jwt.header}")
	private String tokenHeader;

	private JwtTokenUtil jwtTokenUtil;
	
	
	@PutMapping("/{code}/uploadDoc") //complete Order
	public ResponseEntity<OrderDto> completeOrder(@PathVariable String code, 
			@RequestParam(value ="doc", required=false) MultipartFile[] orderFiles){
		
		return ResponseEntity.ok().body(this.orderService.completeOrder(code, orderFiles));
	}
	
	@GetMapping("/email/{code}")
	public ResponseEntity<String> acceptOrder(@PathVariable String code){
		
		return ResponseEntity.ok().body(this.orderService.sendEmail(code));
	}
//	
//	
//	@PutMapping("/{id}/pay")
//	public ResponseEntity<OrderDto> payOrder(@PathVariable Long id){
//		
//		return ResponseEntity.ok().body(this.orderService.payOrder(id));
//	}
	
	@PutMapping("/{code}")
	public ResponseEntity<OrderDto> updateOrderInfo(@PathVariable String code, @RequestBody OrderDto orderDto){
		
		return ResponseEntity.ok().body(this.orderService.updateOrder(code, orderDto));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<OrderDto> getOrderById(@PathVariable Long id){
		return ResponseEntity.ok().body(this.orderService.getOrderById(id));

	}
	
	@GetMapping("/search/{email:.+}")
	public ResponseEntity<List<OrderDto>> getOrderByEmail(@PathVariable("email") String email){
		String result = "";
		try {
			result = java.net.URLDecoder.decode(email, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok().body(this.orderService.getOrdersByEmail(result));
	}
	
	@GetMapping("/list")
	public ResponseEntity<OrderResponseDto>getOrdersByPage(Pageable pageable){
		Page<Order> orderList =  this.orderService.getOrdersByPage(pageable);
	
		List<OrderDto> OrderDtoList = new ArrayList<>();
		for (Order entry :orderList.getContent()) {
			
			OrderDtoList.add(new OrderDto(entry));
		}
		
		OrderResponseDto result = new OrderResponseDto();
		result.setOrderList(OrderDtoList);
		result.setPage(orderList.getNumber());
		result.setSize(orderList.getSize());
		result.setTotalPages(orderList.getTotalPages());
		return  ResponseEntity.ok().body(result);
	}
	
	@GetMapping
	public ResponseEntity<List<OrderDto>> getAllorders(){
		return ResponseEntity.ok().body(this.orderService.getAllOrders());
	}
	
	
	@PostMapping("/{voiceId}")
	public ResponseEntity<OrderDto> createOrder(@PathVariable("voiceId") Long voiceId, HttpServletRequest request){
		
		return ResponseEntity.ok().body(this.orderService.guestBuyNow(voiceId));
		
	} 
	
}
