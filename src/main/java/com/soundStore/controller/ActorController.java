package com.soundStore.controller;
import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.soundStore.dto.ActorDto;
import com.soundStore.dto.VoiceDto;
import com.soundStore.exception.MappingVoiceObjFailed;
import com.soundStore.service.ActorService;
import com.soundStore.service.VoiceService;


@Controller
@RequestMapping(value="/api/actor")
public class ActorController {

	@Autowired
	private ActorService actorService;
	
	
	@PostMapping
	public ResponseEntity<ActorDto> createNewActor(@RequestBody ActorDto actorDto){
		return ResponseEntity.ok().body(this.actorService.createActor(actorDto));
	}
	
	@PostMapping("/{id}/voice")
	public ResponseEntity<VoiceDto> createVoices(@PathVariable("id") Long id, 
			@RequestParam(value ="voiceFile", required=false) MultipartFile voicesFile,
			@RequestParam(value ="voiceDto") String voiceJson){
		VoiceDto voiceDto = null;
		try {
			voiceDto = new ObjectMapper().readValue(voiceJson, VoiceDto.class);
		} catch (IOException e) {
			throw new MappingVoiceObjFailed();
		}
		return ResponseEntity.ok().body(this.actorService.createVoice(voicesFile, id, voiceDto));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ActorDto> getActor(@PathVariable("id") Long id){
		return ResponseEntity.ok().body(this.actorService.getActor(id));
	}
	
	@GetMapping
	public ResponseEntity<List<ActorDto>> getAllActors(){
		return ResponseEntity.ok().body(this.actorService.getAllActors());
	}
	
	@GetMapping("/{id}/voices")
	public ResponseEntity<List<VoiceDto>> getVoicesByActor(@PathVariable("id") Long id){
		
		return ResponseEntity.ok().body(this.actorService.getVoicesByActor(id));
		
	}
	
	@GetMapping("/gender/{gender}")
	public ResponseEntity<List<ActorDto>> getActorsByGender(@PathVariable("gender") String gender){
		return ResponseEntity.ok().body(this.actorService.getActorsByGender(gender));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<ActorDto> editActor(@PathVariable("id") Long id,@RequestBody ActorDto actorDto){
		return ResponseEntity.ok().body(this.actorService.editActor(id, actorDto));
	}
	
	@PutMapping("/{id}/image")
	public ResponseEntity<String> editActor(@PathVariable("id") Long id,
			@RequestParam(value ="image", required=false) MultipartFile image){
		return ResponseEntity.ok().body(this.actorService.setPhotoOfActor(id, image));
	}
	
	
	//TODO DELETE
}
