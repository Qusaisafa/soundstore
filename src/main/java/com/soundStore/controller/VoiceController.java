package com.soundStore.controller;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.soundStore.dto.VoiceDto;
import com.soundStore.exception.MappingVoiceObjFailed;
import com.soundStore.service.VoiceService;

@Controller
@RequestMapping(value="/api/voice")
public class VoiceController {

	@Autowired
	private VoiceService voiceService;
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getVoiceInfo(@PathVariable Long id){
		
		return ResponseEntity.ok().body(this.voiceService.getVoicesById(id));
	}
	
//	@PostMapping("/{id}")
//	public ResponseEntity<VoiceDto> editVoice(@PathVariable("id") Long id, 
//			@RequestParam(value ="voiceFile", required=false) MultipartFile voicesFile,
//			@RequestParam(value ="voiceDto") String voiceJson){
//		VoiceDto voiceDto = null;
//		try {
//			voiceDto = new ObjectMapper().readValue(voiceJson, VoiceDto.class);
//		} catch (IOException e) {
//			throw new MappingVoiceObjFailed();
//		}
//		return ResponseEntity.ok().body(this.voiceService.editVoice(voicesFile, id, voiceDto));
//	}
	
	
//	@GetMapping("/download/{id}")
//	@ResponseBody
//	public ResponseEntity<Resource> getFile(@PathVariable Long id) {
//		Resource file = voiceService.loadFile(id);
//		return ResponseEntity.ok()
//				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
//				.body(file);
//	}

}
