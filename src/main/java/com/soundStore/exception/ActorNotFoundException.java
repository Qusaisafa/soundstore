package com.soundStore.exception;

public class ActorNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ActorNotFoundException(long id) {
		super("Actor Not Found : " + id);
	}
	
	public ActorNotFoundException() {
		super("no actor saved");
	}

}