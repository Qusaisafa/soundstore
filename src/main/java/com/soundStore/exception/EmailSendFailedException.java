package com.soundStore.exception;

public class EmailSendFailedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public EmailSendFailedException() {
		super("sending email to user failed");
	}

}