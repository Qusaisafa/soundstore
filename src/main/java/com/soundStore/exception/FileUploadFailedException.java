package com.soundStore.exception;

public class FileUploadFailedException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FileUploadFailedException(String file) {
		super("saving file falied :  " + file);
	}
	
	public FileUploadFailedException() {
		super("documents upload and save is failed ");
	}
	
}