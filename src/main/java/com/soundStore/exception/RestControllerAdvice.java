package com.soundStore.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class RestControllerAdvice {
	
	@ExceptionHandler(MappingVoiceObjFailed.class)
	@ResponseBody
	@ResponseStatus(code = HttpStatus.CONFLICT)
	public ResponseEntity<String> MappingVoiceObjFailed(Exception ex) {
		ex.printStackTrace();
		return new ResponseEntity<String>(ex.getMessage(), HttpStatus.CONFLICT);
	}
	
	
	@ExceptionHandler(ActorNotFoundException.class)
	@ResponseBody
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public ResponseEntity<String> handleActorNotFoundException(Exception ex) {
		ex.printStackTrace();
		return new ResponseEntity<String>(ex.getMessage(), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(CartNotFoundException.class)
	@ResponseBody
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public ResponseEntity<String> handleCartNotFoundException(Exception ex) {
		ex.printStackTrace();
		return new ResponseEntity<String>(ex.getMessage(), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(FileUploadFailedException.class)
	@ResponseBody
	@ResponseStatus(code = HttpStatus.CONFLICT)
	public ResponseEntity<String> FileUploadFailedException(Exception ex) {
		ex.printStackTrace();
		return new ResponseEntity<String>(ex.getMessage(), HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(VoiceNotFoundException.class)
	@ResponseBody
	@ResponseStatus(code = HttpStatus.CONFLICT)
	public ResponseEntity<String> VoiceNotFoundException(Exception ex) {
		ex.printStackTrace();
		return new ResponseEntity<String>(ex.getMessage(), HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(UserNotFoundException.class)
	@ResponseBody
	@ResponseStatus(code = HttpStatus.CONFLICT)
	public ResponseEntity<String> UserNotFoundException(Exception ex) {
		ex.printStackTrace();
		return new ResponseEntity<String>(ex.getMessage(), HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(OrderNotFoundException.class)
	@ResponseBody
	@ResponseStatus(code = HttpStatus.CONFLICT)
	public ResponseEntity<String> OrderNotFoundException(Exception ex) {
		ex.printStackTrace();
		return new ResponseEntity<String>(ex.getMessage(), HttpStatus.CONFLICT);
	}
	
}
