package com.soundStore.exception;

public class MappingVoiceObjFailed extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MappingVoiceObjFailed() {
		super("inncorrect voice json");
	}
}
