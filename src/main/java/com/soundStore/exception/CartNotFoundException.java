package com.soundStore.exception;

public class CartNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CartNotFoundException(Long id) {
		super("cart not found with id " + id);
	}

}