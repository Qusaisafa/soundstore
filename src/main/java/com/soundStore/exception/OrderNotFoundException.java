package com.soundStore.exception;

public class OrderNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OrderNotFoundException(long id) {
		super("order Not Found with id " + id);
	}
	
	public OrderNotFoundException() {
		super("no orders exist ");
	}
	

}