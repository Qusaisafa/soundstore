package com.soundStore.exception;

public class VoiceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public VoiceNotFoundException(long id) {
		super("voice Not Found with id " + id);
	}
	
	public VoiceNotFoundException() {
		super("no voices exist");
	}
}