package com.soundStore.dto;

import java.util.Date;

import com.soundStore.model.Actor;

public class ActorDto {
	
	
	private Long id;
	
	private String name;
	
	private String gender;
	
	private String age ;
	
	private Date createdDate;
	
	private boolean active;

	private String imageUrl;
	
	public ActorDto() {
		
	}
	
	public ActorDto(Actor actor) {
		this.id =  actor.getId();
		this.gender = actor.getGender();
		this.age = actor.getAge();
		this.name = actor.getName();
		this.active = actor.isActive();
		this.createdDate = actor.getCreatedDate();
		this.setImageUrl(actor.getImageUrl());
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	

//	public List<VoiceDto> getVoices() {
//		return voices;
//	}
//
//	public void setVoices(List<VoiceDto> voices) {
//		this.voices = voices;
//	}
	
}
