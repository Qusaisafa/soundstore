package com.soundStore.dto;

import java.util.ArrayList;
import java.util.List;

public class OrderResponseDto {
	
	private List<OrderDto> OrderList = new ArrayList<>();
	
	private long size;
	
	private long totalPages;
	
	private long page;
	
//	private String sort;
	
//	private long totalElements;
	
//	private long element;
	
	public OrderResponseDto() {
		
	}
	

	public List<OrderDto> getOrderList() {
		return OrderList;
	}

	public void setOrderList(List<OrderDto> orderList) {
		OrderList = orderList;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
	}

	public long getPage() {
		return page;
	}

	public void setPage(long page) {
		this.page = page;
	}

//	public String getSort() {
//		return sort;
//	}
//
//	public void setSort(String sort) {
//		this.sort = sort;
//	}
//
//
//	public long getTotalElements() {
//		return totalElements;
//	}
//
//
//	public long getElement() {
//		return element;
//	}
//
//
//	public void setElement(long element) {
//		this.element = element;
//	}
//
//
//	public void setTotalElements(long totalElements2) {
//		this.totalElements = totalElements;
//		
//	}
	
	
}
