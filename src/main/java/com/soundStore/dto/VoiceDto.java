package com.soundStore.dto;

import com.soundStore.model.Category;
import com.soundStore.model.Voice;

public class VoiceDto {
	private Long id;
	
	private String name;
	
	private String url;
	
	private String category; 
	
//	private String status;
	
//	private String description;
	
//	private String length;
	
//	private int rate;
	
//	private long buyCount;
	
	public VoiceDto() {
		
	}
	
	public VoiceDto(Voice voice) {
		this.id = voice.getId();
		this.name = voice.getName();
		this.category = voice.getCategory();
		this.url = voice.getUrl();
	}
	

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
