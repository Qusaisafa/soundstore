//package com.soundStore.dto;
//
//
//import java.util.ArrayList;
//import java.util.List;
//
//import com.soundStore.model.Cart;
//import com.soundStore.model.Order;
//
//public class CartDto {
//	private Long id;
//	
//	private List<OrderDto> orders = new ArrayList<>();
//	
//	private long userId;
//	
//	public CartDto() {
//		
//	}
//	
//	public CartDto(Cart cart) {
//		this.id = cart.getId();
//		this.userId = cart.getUser().getId();
//		for(Order entry : cart.getOrders()) {
//			this.orders.add(new OrderDto(entry));
//		}
//	}
//	
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public List<OrderDto> getOrders() {
//		return orders;
//	}
//
//	public void setOrders(List<OrderDto> orders) {
//		this.orders = orders;
//	}
//
//	public long getUserId() {
//		return userId;
//	}
//
//	public void setUserId(long userId) {
//		this.userId = userId;
//	}
//
//	
//	
//}
