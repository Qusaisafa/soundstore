package com.soundStore.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.soundStore.model.Order;
import com.soundStore.model.Voice;

public class OrderDto {
	
	private Long id;
	
	private String[] DocUrls;
	
	private long price;
	
	private boolean paied;
	
	private boolean completed;
	
	private boolean acctepted;
	
	private Long userId;
	
	private Long cartId;
	
	private String guestName;
	
	private String guestEmail;

	private String description;
	
	private String code;
	
	private String notes;
	
	private String minutes;
	
	private String projects;
	
	private Date createdDate;
	
	
	private List<VoiceDto> voices = new ArrayList<>();
	
	public OrderDto() {
		
	}
	
	public OrderDto(Order order) {
		this.id = order.getId();
		this.DocUrls = order.getDocUrls();
		this.completed = order.isCompleted();
		this.paied = order.isPaied();
		this.price = order.getPrice();
//		this.userId = order.getUser().getId();
		this.acctepted = order.isAccepted();
		this.guestEmail = order.getGuestEmail();
		this.guestName = order.getGuestName();
		this.description = order.getDescription();
		this.code = order.getCode();
		this.notes = order.getNotes();
		this.minutes = order.getMinutes();
		this.projects = order.getProjects();
		this.createdDate = order.getCreatedDate();
		for(Voice entry : order.getVoices()) {
			this.voices.add(new VoiceDto(entry));
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
//
//	public String getDocUrl() {
//		return DocUrl;
//	}
//
//	public void setDocUrl(String docUrl) {
//		DocUrl = docUrl;
//	}
	

	public long getPrice() {
		return price;
	}


	public String[] getDocUrls() {
		return DocUrls;
	}

	public void setDocUrls(String[] docUrls) {
		DocUrls = docUrls;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public boolean isPaied() {
		return paied;
	}

	public void setPaied(boolean paied) {
		this.paied = paied;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public boolean isAcctepted() {
		return acctepted;
	}

	public void setAcctepted(boolean acctepted) {
		this.acctepted = acctepted;
	}

	public List<VoiceDto> getVoices() {
		return voices;
	}

	public void setVoices(List<VoiceDto> voices) {
		this.voices = voices;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getGuestEmail() {
		return guestEmail;
	}

	public void setGuestEmail(String guestEmail) {
		this.guestEmail = guestEmail;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getMinutes() {
		return minutes;
	}

	public void setMinutes(String minutes) {
		this.minutes = minutes;
	}

	public String getProjects() {
		return projects;
	}

	public void setProjects(String projects) {
		this.projects = projects;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
