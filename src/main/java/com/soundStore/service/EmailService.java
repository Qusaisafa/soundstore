package com.soundStore.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.context.Context;

import com.soundStore.model.Mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Hashtable;
import java.util.Map;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

//    @Autowired
//    private SpringTemplateEngine templateEngine;
//    
    @Autowired
	private Configuration freemarkerConfig;


    public void sendSimpleMessage(Mail email) throws IOException, TemplateException, MessagingException {
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message,
		            MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
		            StandardCharsets.UTF_8.name());
		
		
//	    helper.addAttachment("12331522050090454.png", new ClassPathResource("12331522050090454.png"));
//	    helper.addAttachment("64951510234941531.png", new ClassPathResource("classpath:/resources/64951510234941531.png"));
//	    helper.addAttachment("67611522142640957.png", new ClassPathResource("classpath:/resources/67611522142640957.png"));
//	    helper.addAttachment("77981522050090360.png", new ClassPathResource("classpath:/resources/77981522050090360.png"));
//	    helper.addAttachment("logo.png", new ClassPathResource("logo.png"));

		Template t = freemarkerConfig.getTemplate("email.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, email.getModel());
		
		helper.setTo(email.getTo());
		helper.setText(html, true);
		helper.setSubject(email.getSubject());
		helper.setFrom(email.getFrom());
		helper.setBcc(email.getBcc());
		emailSender.send(message);
	}

}