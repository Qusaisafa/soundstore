package com.soundStore.service;


import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileService {
	
	@Value("${doc.file.path}")
	private Path filePath;
	
	
	public Resource loadFile(String filename) {
		try {
			Path file = filePath.resolve(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("fail to load file");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("fail to load file");
		}
	}
}
