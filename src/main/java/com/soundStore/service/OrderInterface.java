package com.soundStore.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.soundStore.model.Order;

public interface OrderInterface {

	public Page<Order> getOrdersByPage(Pageable pageable);
	
}
