package com.soundStore.service;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.soundStore.auth.repository.UserRepository;
import com.soundStore.dto.OrderDto;
import com.soundStore.exception.EmailSendFailedException;
import com.soundStore.exception.FileUploadFailedException;
import com.soundStore.exception.OrderNotFoundException;
import com.soundStore.exception.VoiceNotFoundException;
import com.soundStore.model.Mail;
import com.soundStore.model.Order;
import com.soundStore.model.Voice;
import com.soundStore.repository.OrderRepository;
import com.soundStore.repository.VoiceRepository;
import com.soundStore.utils.URLUtils;

@Service
public class OrderService {
	
	@Value("${doc.file.path}")
	private String docsOrderPath;
	
	@Value("${backend.url}")
	private String backendUrl;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private VoiceRepository voiceRepository;
	
	@Autowired
    private EmailService emailService;
	
	@Transactional
	public OrderDto completeOrder(String code, MultipartFile[]  orderDocs) {
		
		Order order = this.orderRepository.findByCode(code);
		if(order == null) {
			throw new OrderNotFoundException();
		}
		
//		order.setPaied(false);
//		order.setAccepted(false);
//		order.setCompleted(true);
		
		int currentLength = 0;
		if(order.getDocUrls() == null) {
			order.setDocUrls(new String[1]);
			
		}else {
			currentLength = order.getDocUrls().length;
			
		}
		
		List<String> urls = new ArrayList<String>(Arrays.asList(order.getDocUrls()));
		
		if(orderDocs != null) {
			for(int i=0;i<orderDocs.length;i++) {
				try {
					String extension = orderDocs[i].getOriginalFilename().split("\\.")[1];
					String generatedName = this.generateOrderName(extension);
					String path = this.saveFile(orderDocs[i], generatedName);
					urls.add(backendUrl+generatedName);
				}catch (Exception e) {
					e.printStackTrace();
					throw new FileUploadFailedException(orderDocs[i].getOriginalFilename());
				}
			}
			order.setDocUrls(urls.toArray(new String[orderDocs.length + currentLength]));
		}
	
		order = this.orderRepository.save(order);
		return new OrderDto(order);
	}
	
	@Transactional
	public String sendEmail(String code) {
		Order order = this.orderRepository.findByCode(code);
		if(order == null) {
			throw new OrderNotFoundException();
		}
		
		try {

			Mail mail = new Mail();
	        mail.setFrom("info@thearabvoiceover.com");
	        mail.setBcc("info@thearabvoiceover.com");
	        mail.setTo(order.getGuestEmail());
	        mail.setSubject("Order code "+order.getCode());

	        Map<String, Object> model = new HashMap<>();
	        model.put("code", order.getCode());
	        model.put("name", order.getGuestName());
	        model.put("email", order.getGuestEmail());
	        model.put("n", order.getProjects());
	        model.put("m", order.getMinutes());
	        if(order.getPrice() == 0)
	        	 model.put("price", "N/A");
	        else
	        	model.put("price", "$"+order.getPrice());


	        mail.setModel(model);

	        emailService.sendSimpleMessage(mail);
	        
		    }catch(Exception e) {
			e.printStackTrace();
			throw new EmailSendFailedException();
		}
		
		
		return "email send";
	}
	
	@Transactional
	public OrderDto updateOrder(String code, OrderDto orderDto) {
		Order order = this.orderRepository.findByCode(code);
		if(order == null) {
			throw new OrderNotFoundException();
		}
		
		order.setDescription(orderDto.getDescription());
		order.setGuestEmail(orderDto.getGuestEmail());
		order.setGuestName(orderDto.getGuestName());
		order.setPrice(orderDto.getPrice());
		order.setMinutes(orderDto.getMinutes());
		order.setNotes(orderDto.getNotes());
		order.setProjects(orderDto.getProjects());
		order.setCompleted(orderDto.isCompleted());
		order.setPaied(orderDto.isPaied());
		order.setAccepted(order.isAccepted());
		order = this.orderRepository.save(order);
		return new OrderDto(order);
		
	}
	
	@Transactional
	public OrderDto payOrder(Long id) {
		Order order = this.orderRepository.findOne(id);
		if(order == null) {
			throw new OrderNotFoundException(id);
		}
		
		order.setPaied(true);
		order = this.orderRepository.save(order);
		return new OrderDto(order);
		
	}
	
	
	public OrderDto getOrderById(Long id) {
		Order order = this.orderRepository.findOne(id);
		
		if(order == null) {
			throw new OrderNotFoundException(id);
		}
		order = this.orderRepository.save(order);
		return new OrderDto(order);
	}
	
	public Page<Order> getOrdersByPage(Pageable pageable) {
		PageRequest pg = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.DESC, "createdDate" );
		return this.orderRepository.findAll(pg);
	}
	
	private String generateOrderName(String extension) {
		return new String("Order_" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()) + "." + extension)
				.replace(":", "").replace(" ", "");
	}
	
	
	@Transactional
	public OrderDto guestBuyNow(Long voiceId) {
		
		Voice voice = this.voiceRepository.findOne(voiceId);
		
		if(voice == null) {
			throw new VoiceNotFoundException(voiceId);
		}
		
		Order order = new Order();
		order.getVoices().add(voice);
		order.setCompleted(false);
		order.setPaied(false);
		order.setCode(URLUtils.generateMyNumber());
		order.setCreatedDate(new Date());
		order.setMinutes("");
		order.setNotes("");
		order.setProjects("");
		//order.setPrice(price); TODO
		order = this.orderRepository.save(order);
		return new OrderDto(order);
	}
	
	
	private String saveFile(MultipartFile file, String fileName) throws IOException {
		byte[] bytes = file.getBytes();
		String imagePath = new String(this.docsOrderPath +"/"+ fileName);
		Path path = Paths.get(imagePath);
		Files.write(path, bytes);
		return imagePath;
	}

	public List<OrderDto> getOrdersByEmail(String email) {
		
		List<Order> orderList = this.orderRepository.findByGuestEmail(email);
		List<OrderDto> orderDtoList = new ArrayList<>();
		
		if(orderList == null) {
			throw new OrderNotFoundException();
		}
		
		for(Order entry:orderList) {
			OrderDto newOrder = new OrderDto(entry);
			orderDtoList.add(newOrder);
		}
		return orderDtoList;
		
	}

	public List<OrderDto> getAllOrders() {
		List<Order> orderList = this.orderRepository.findAll();
		List<OrderDto> orderDtoList = new ArrayList<>();
		
		if(orderList == null) {
			throw new OrderNotFoundException();
		}
		
		for(Order entry:orderList) {
			OrderDto newOrder = new OrderDto(entry);
			orderDtoList.add(newOrder);
		}
		return orderDtoList;	}

}
