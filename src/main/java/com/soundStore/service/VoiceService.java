package com.soundStore.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.soundStore.dto.VoiceDto;
import com.soundStore.exception.ActorNotFoundException;
import com.soundStore.exception.FileUploadFailedException;
import com.soundStore.exception.VoiceNotFoundException;
import com.soundStore.model.Actor;
import com.soundStore.model.Voice;
import com.soundStore.repository.ActorRepository;
import com.soundStore.repository.VoiceRepository;


@Service
public class VoiceService {
	
	@Value("${doc.file.path}")
	private Path voiceFilePath;
	
	@Autowired 
	private VoiceRepository voiceRepository;
	
	@Value("${backend.url}")
	private String backendUrl;
	
	
	@Transactional
	public List<VoiceDto> getVoices() {
		List<Voice> voices = this.voiceRepository.findAll();
		
		if(voices == null) {
			throw new VoiceNotFoundException();
		}
		
		List<VoiceDto> voiceDtoList = new ArrayList<>();
		for(Voice entry: voices) {
			VoiceDto voiceDto = new VoiceDto();
			voiceDto.setId(entry.getId());
			voiceDto.setName(entry.getName());
			voiceDto.setUrl(entry.getUrl());
			voiceDtoList.add(voiceDto);
		}
		
		return voiceDtoList;
	}
	
	
	@Transactional
	public VoiceDto getVoicesById(Long id) {
		Voice voice = this.voiceRepository.findOne(id);
		if(voice == null) {
			throw new VoiceNotFoundException(id);
		}
		
		VoiceDto voiceDto = new VoiceDto();
		voiceDto.setId(voice.getId());
		voiceDto.setName(voice.getName());
		voiceDto.setUrl(voice.getUrl());
		voiceDto.setCategory(voice.getCategory());

		return voiceDto;
	}


	public VoiceDto editVoice(MultipartFile voicesFile, Long id, VoiceDto voiceDto) {
		
		Voice voice = this.voiceRepository.findOne(id);
		
		if(voice == null) {
			throw new VoiceNotFoundException(id);
		}
	
		try {
			String extension = voicesFile.getOriginalFilename().split("\\.")[1];
			String generatedName = this.generateImageName(extension);
			String path = this.saveFile(voicesFile, generatedName);
			voice.setUrl(backendUrl+generatedName);
			voice.setName(voiceDto.getName());
			voice.setCategory(voiceDto.getCategory());
			voice = this.voiceRepository.save(voice);
		}catch (Exception e) {
			e.printStackTrace();
			throw new FileUploadFailedException(voicesFile.getOriginalFilename());
		}
		
		VoiceDto voiceDtoResult = new VoiceDto();
		voiceDtoResult.setName(voice.getName());
		voiceDtoResult.setId(voice.getId());
		voiceDtoResult.setUrl(voice.getUrl());
		voiceDtoResult.setCategory(voice.getCategory());
		return voiceDtoResult;
	}
	
	private String generateImageName(String extension) {
		return new String("voice_" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()) + "." + extension)
				.replace(":", "").replace(" ", "");
	}
	
	private String saveFile(MultipartFile file, String fileName) throws IOException {
		byte[] bytes = file.getBytes();
		String imagePath = new String(voiceFilePath+("/")+ fileName);
		Path path = Paths.get(imagePath);
		Files.write(path, bytes);
		return imagePath;
	}


	public Resource loadFile(Long id) {
		
		Voice voice = this.voiceRepository.findOne(id);
		
		if(voice == null) {
			throw new VoiceNotFoundException(id);
		}
		try {
			Path file = voiceFilePath.resolve(voice.getUrl());
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("fail to load");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("fail to load");
		}
	}
 
	
}
