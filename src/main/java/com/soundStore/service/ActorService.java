package com.soundStore.service;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.soundStore.dto.ActorDto;
import com.soundStore.dto.VoiceDto;
import com.soundStore.exception.ActorNotFoundException;
import com.soundStore.exception.FileUploadFailedException;
import com.soundStore.model.Actor;
import com.soundStore.model.Voice;
import com.soundStore.repository.ActorRepository;
import com.soundStore.repository.VoiceRepository;

@Service
public class ActorService {
	
	@Value("${doc.file.path}")
	private String filePath;
	
	@Value("${backend.url}")
	private String backendUrl;
	
	@Autowired
	private ActorRepository actorRepository;
	
	@Autowired 
	private VoiceRepository voiceRepository;
	
	
	@Transactional
	public ActorDto createActor(ActorDto actorDto) {
		
		Actor actor = new Actor();
		
		actor.setAge(actorDto.getAge());
		actor.setCreatedDate(new Date());
		actor.setGender(actorDto.getGender());
		actor.setName(actorDto.getName());
		actor.setActive(actorDto.isActive());
		actor = this.actorRepository.save(actor);
		

		return new ActorDto(actor);
		
	}
	
	
	@Transactional
	public String setPhotoOfActor(long id, MultipartFile image) {
		Actor actor = this.actorRepository.findOne(id);
		if(actor == null) {
			throw new ActorNotFoundException(id);
		}
		String url = "";
		if(image!=null) {
			try {
				String extension = image.getOriginalFilename().split("\\.")[1];
				String generatedName = this.generateImageName(extension);
				String path = this.saveImage(image, generatedName);
				url = backendUrl+generatedName;
				actor.setImageUrl(url);
			}catch (Exception e) {
				e.printStackTrace();
				throw new FileUploadFailedException(image.getOriginalFilename());
			}
		}
		actor = this.actorRepository.save(actor);
		return  url;
	}
	
	@Transactional
	public ActorDto getActor(Long id) {
		
		Actor actor = this.actorRepository.findOne(id);
		if(actor == null) {
			throw new ActorNotFoundException(id);
		}

		return new ActorDto(actor);
	}

	public List<ActorDto> getAllActors() {
		List<Actor> actors = this.actorRepository.findAll();
		List<ActorDto> actorDtoList = new ArrayList<>();
		
		if(actors == null) {
			throw new ActorNotFoundException();
		}
		
		for(Actor entry:actors) {
			ActorDto newActor = new ActorDto(entry);
			actorDtoList.add(newActor);
		}
		return actorDtoList;
	}

	public List<ActorDto> getActorsByGender(String gender) {
		List<Actor> actors = this.actorRepository.findByGender(gender);
		List<ActorDto> actorDtoList = new ArrayList<>();
		
		if(actors == null) {
			throw new ActorNotFoundException();
		}
		
		for(Actor entry:actors) {
			ActorDto newActor = new ActorDto(entry);
			actorDtoList.add(newActor);
		}
		return actorDtoList;
	}

	@Transactional
	public VoiceDto createVoice(MultipartFile voiceFiles , Long actorId, VoiceDto voiceDto) {
		
		Voice voice = null;
		
		Actor actor = this.actorRepository.findOne(actorId);
		if(actor == null) {
			throw new ActorNotFoundException(actorId);
		}
	
		try {
			voice = new Voice();
			if(voiceFiles!=null) {
				String extension = voiceFiles.getOriginalFilename().split("\\.")[1];
				String generatedName = this.generateVoiceName(extension);
				String path = this.saveFile(voiceFiles, generatedName);
				voice.setUrl(backendUrl+generatedName);
			}
			voice.setName(voiceDto.getName());
			voice.setCategory(voiceDto.getCategory());
			voice.setActor(actor);
			voice = this.voiceRepository.save(voice);
		}catch (Exception e) {
			e.printStackTrace();
			throw new FileUploadFailedException(voiceFiles.getOriginalFilename());
		}
		
		VoiceDto voiceDtoResult = new VoiceDto();
		voiceDtoResult.setName(voice.getName());
		voiceDtoResult.setId(voice.getId());
		voiceDtoResult.setUrl(voice.getUrl());
		voiceDtoResult.setCategory(voice.getCategory());
		return voiceDtoResult;
	}
	
	private String generateVoiceName(String extension) {
		return new String("voice_" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()) + "." + extension)
				.replace(":", "").replace(" ", "");
	}
	
	private String generateImageName(String extension) {
		return new String("photo_" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()) + "." + extension)
				.replace(":", "").replace(" ", "");
	}
	
	private String saveFile(MultipartFile file, String fileName) throws IOException {
		byte[] bytes = file.getBytes();
		String imagePath = new String(this.filePath + "/" + fileName);
		Path path = Paths.get(imagePath);
		Files.write(path, bytes);
		return imagePath;
	}
	private String saveImage(MultipartFile file, String fileName) throws IOException {
		byte[] bytes = file.getBytes();
		String imagePath = new String(this.filePath + "/"+ fileName);
		Path path = Paths.get(imagePath);
		Files.write(path, bytes);
		return imagePath;
	}

	@Transactional
	public List<VoiceDto> getVoicesByActor(Long actorId) {
		Actor actor = this.actorRepository.findOne(actorId);
		if(actor == null) {
			throw new ActorNotFoundException(actorId);
		}
		
		List<VoiceDto> voiceDtoList = new ArrayList<>();
		for(Voice entry: actor.getVoices()) {
			VoiceDto voiceDto = new VoiceDto();
			voiceDto.setId(entry.getId());
			voiceDto.setName(entry.getName());
			voiceDto.setCategory(entry.getCategory());
			voiceDto.setUrl(entry.getUrl());
			voiceDtoList.add(voiceDto);
		}
		
		return voiceDtoList;
	}
	
	@Transactional
	public ActorDto editActor(Long id ,ActorDto actorDto) {
		Actor actor = this.actorRepository.findOne(id);
		if(actor == null) {
			throw new ActorNotFoundException(id);
		}
		
		actor.setAge(actorDto.getAge());
		actor.setGender(actorDto.getGender());
		actor.setName(actorDto.getName());
		actor.setActive(actorDto.isActive());
		actor = this.actorRepository.save(actor);
		
		return new ActorDto(actor);
		
	}
	public void init() {
		try {
			Path path = Paths.get(filePath); 
			Files.createDirectory(path);
		} catch (IOException e) {
			throw new RuntimeException("Could not initialize storage!");
		}
	}
	
	
//	@Transactional //TODO
//	public String deleteActor(Long id) {
//		
//	}
}
