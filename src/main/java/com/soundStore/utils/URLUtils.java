package com.soundStore.utils;

import javax.servlet.http.HttpServletRequest;

public class URLUtils {

	public static String getBaseURl(HttpServletRequest request) {
	    String scheme = request.getScheme();
	    String serverName = request.getServerName();
	    int serverPort = request.getServerPort();
	    String contextPath = request.getContextPath();
	    StringBuffer url =  new StringBuffer();
	    url.append(scheme).append("://").append(serverName);
	    if ((serverPort != 80) && (serverPort != 443)) {
	        url.append(":").append(serverPort);
	    }
	    url.append(contextPath);
	    if(url.toString().endsWith("/")){
	    	url.append("/");
	    }
	    return url.toString();
	}
	
	
	public static String generateMyNumber()
	{
		int aNumber = 0; 
		aNumber = (int)((Math.random() * 9000000)+1000000); 
		return Integer.toString(aNumber);
	}
	
}
