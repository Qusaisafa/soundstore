package com.soundStore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.soundStore.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long>{

//	List<Order> findByCompletedAndUserId(boolean completed, Long userId);
//	List<Order> findByAcceptedAndUserId(boolean accepted, Long userId);
	
	List<Order> findByGuestEmail(String guestEmail);
	
	Order findByCode(String code);
}
