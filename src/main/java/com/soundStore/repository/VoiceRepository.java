package com.soundStore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.soundStore.model.Voice;


public interface VoiceRepository extends JpaRepository<Voice, Long>{

}
