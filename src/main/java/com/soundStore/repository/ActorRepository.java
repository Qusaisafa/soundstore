package com.soundStore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.soundStore.model.Actor;


public interface ActorRepository extends JpaRepository<Actor, Long>{

	List<Actor> findByGender(String gender);
}
